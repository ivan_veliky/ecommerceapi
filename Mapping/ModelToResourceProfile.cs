using AutoMapper;
using ECommerce.Resources;
using ECommerce.Domains.Models;

namespace ECommerce.Mapping 
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Category, CategoryResource>();
            CreateMap<Product, ProductResource>();
            CreateMap<Cart, CartResource>();
            CreateMap<CartItem, CartItemResource>();
            CreateMap<CartItem, CartItemResource>();
        }
    }
}