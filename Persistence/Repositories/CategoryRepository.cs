using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

using ECommerce.Domains.Models;
using ECommerce.Domains.Repositories;
using ECommerce.Persistence.Contexts;

namespace ECommerce.Persistence.Repositories 
{
    public class CategoryRepository : BaseRepository, ICategoryRepository 
    {
        public CategoryRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Category>> GetAllAsync() 
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task<Category> GetAsync(int id) 
        {
            try {
                return await _context.Categories.SingleAsync<Category>(c => c.Id == id);
            }
            catch(System.InvalidOperationException e) { // TODO: add logging
                return null;
            }
            catch(System.Exception e) {
                return null;
            }
        }

        public async Task<IEnumerable<Product>> GetProducts(int catId) 
        {
            return await (from p in _context.Products where p.CategoryId == catId select p).Include(p => p.Category).ToListAsync();
        }
    }
}