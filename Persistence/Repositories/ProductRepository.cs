using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

using ECommerce.Domains.Models;
using ECommerce.Domains.Repositories;
using ECommerce.Persistence.Contexts;

namespace ECommerce.Persistence.Repositories 
{
    public class ProductRepository : BaseRepository, IProductRepository 
    {
        public ProductRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Product>> GetAllAsync() 
        {
            return await _context.Products.Include(p => p.Category).ToListAsync();
        }

        public async Task<Product> GetAsync(int id) 
        {
            try {
                return await _context.Products.Include(p => p.Category).SingleAsync<Product>(p => p.Id == id);
            } 
            catch(System.InvalidOperationException e) { // TODO: add error logging
                return null;
            }
            catch(System.Exception e) {
                return null;
            }
        }

        public async Task<IEnumerable<Product>> GetByCategory(string category) {
            Category cat = await _context.Categories.SingleAsync<Category>(c => c.Name == category);
            return await (from p in _context.Products where p.CategoryId == cat.Id select p).ToListAsync();  
        }
    }
}