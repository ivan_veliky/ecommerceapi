using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

using ECommerce.Domains.Models;
using ECommerce.Domains.Repositories;
using ECommerce.Persistence.Contexts;
using ECommerce.Domains.Services;

namespace ECommerce.Persistence.Repositories
{
     /*
      * Class provides abstract api to work with storage
      *
      * TODO: move CartItems methods to seperate CartItemsRepository class
      */
    public class CartRepository : BaseRepository, ICartRepository
    {
        public CartRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Cart> GetAsync(int id)
        {
            try
            {
                return await (from c in _context.Carts where c.Id == id select c)
                .Include(c => c.CartItems).ThenInclude(i => i.Product).SingleAsync();
            }
            catch (System.InvalidOperationException e)
            { // TODO: add logging
                return null;
            }
            catch (System.Exception e)
            {
                return null;
            }
        }

        public async Task<CartItem> GetItemAsync(int id)
        {
            try
            {
               return await _context.CartItems.Include(i => i.Product).SingleAsync<CartItem>(i => i.Id == id);
            }
            catch (System.InvalidOperationException e)
            { // TODO: add logging
                return null;
            }
            catch (System.Exception e)
            {
                return null;
            }
        }

        public async Task<CartItem> GetItemByProductIdAsync(int prodId)
        {
            try
            {
               return await _context.CartItems.Include(i => i.Product).SingleAsync<CartItem>(i => i.ProductId == prodId);
            }
            catch (System.InvalidOperationException e)
            { // TODO: add logging
                return null;
            }
            catch (System.Exception e)
            {
                return null;
            }
        }

        public async void Update(CartItem item) {
            _context.CartItems.Update(item);
            await _context.SaveChangesAsync();
        }

        public async Task<Cart> FindByIdAsync(int id)
        {
            return await (from c in _context.Carts where c.Id == id select c).Include(cart => cart.CartItems).SingleAsync();
        }

        public void AddItemAsync(CartItem item)
        {
            _context.CartItems.AddAsync(item);
            _context.SaveChangesAsync();
        }

        public void RemoveItemAsync(CartItem item)
        {
            _context.CartItems.Remove(item);
            _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<CartItem>> GetItemsAsync(int id) 
        {
            return await (from c in _context.CartItems where c.CartId == id select c).Include(i => i.Product).ToListAsync();
        }
    }
}