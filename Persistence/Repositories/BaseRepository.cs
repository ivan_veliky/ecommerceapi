using ECommerce.Persistence.Contexts;

namespace ECommerce.Persistence.Repositories 
{
    public abstract class BaseRepository 
    {
        protected readonly AppDbContext _context;

        public BaseRepository(AppDbContext context) 
        {
            _context = context;
        }
    }
}