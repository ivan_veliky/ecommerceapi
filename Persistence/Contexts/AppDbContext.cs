using Microsoft.EntityFrameworkCore;
using ECommerce.Domains.Models;
using System.Collections.Generic;

namespace ECommerce.Persistence.Contexts
{
    /*
    * Class handles EF configurations and setting up mock data
    */
    public class AppDbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CartItem> CartItems { get; set; }

        public DbSet<Cart> Carts { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Category>().ToTable("Categories");
            builder.Entity<Category>().HasKey(cat => cat.Id);
            builder.Entity<Category>().Property(cat => cat.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Category>().Property(cat => cat.Name).IsRequired().HasMaxLength(30);
            builder.Entity<Category>().HasMany(cat => cat.Products).WithOne(product => product.Category).HasForeignKey(product => product.CategoryId);

            // some in-memory data for categories
            //TODO: integrate db
            builder.Entity<Category>().HasData
            (
                new Category { Id = 100, Name = "Smartphones" },
                new Category { Id = 101, Name = "Laptops" }
            );

            builder.Entity<Cart>().ToTable("Carts");
            builder.Entity<Cart>().HasKey(cart => cart.Id);
            builder.Entity<Product>().Property(cart => cart.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Cart>().HasMany(cart => cart.CartItems).WithOne(item => item.Cart).HasForeignKey(item => item.CartId);

            builder.Entity<Cart>().HasData
            (
                new Cart { Id = 1 }
            );

            builder.Entity<CartItem>().ToTable("CartItems");
            builder.Entity<CartItem>().HasKey(item => item.Id);
            builder.Entity<CartItem>().Property(item => item.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<CartItem>().Property(item => item.Quantity).HasDefaultValue(1);

            builder.Entity<Product>().ToTable("Products");
            builder.Entity<Product>().HasKey(product => product.Id);
            builder.Entity<Product>().Property(product => product.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Product>().HasMany(prod => prod.CartItems).WithOne(item => item.Product).HasForeignKey(item => item.ProductId);


            // some in-memory data for products 
            //TODO: integrate db
            builder.Entity<Product>().HasData
            (
                new Product
                {
                    Id = 1000,
                    Name = "Huawei P30",
                    CategoryId = 100,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                },
                new Product
                {
                    Id = 1001,
                    Name = "Xiaomi Mi 8",
                    CategoryId = 100,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                },
                new Product
                {
                    Id = 1002,
                    Name = "Acer Aspire",
                    CategoryId = 101,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                },
                new Product
                {
                    Id = 1003,
                    Name = "Lenovo IdeaPad",
                    CategoryId = 101,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                },
                new Product
                {
                    Id = 1005,
                    Name = "Nokia Xperia",
                    CategoryId = 100,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                },
                new Product
                {
                    Id = 1007,
                    Name = "Asus Zenbook",
                    CategoryId = 101,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                },
                new Product
                {
                    Id = 1008,
                    Name = "MacBook Pro",
                    CategoryId = 101,
                    Description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                }
            );
        }
    }
}