using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ECommerce.Domains.Services;
using ECommerce.Domains.Repositories;
using ECommerce.Domains.Models;

namespace ECommerce.Services 
{
      /*
    * Class handles all the logic for controllers
    */
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            this._categoryRepository = categoryRepository;
        }

        public async Task<IEnumerable<Category>> GetAllAsync() 
        {
            return await _categoryRepository.GetAllAsync();
        }

        public async Task<Category> GetAsync(int id) 
        {
            if(id == 0) {
                return null;
            }
            return await _categoryRepository.GetAsync(id);
        }

        public async Task<IEnumerable<Product>> GetProducts(int catId) 
        {
            if(catId == 0) {
                return null;
            }
            return await _categoryRepository.GetProducts(catId);
        }
    }
}