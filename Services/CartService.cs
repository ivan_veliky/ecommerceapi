using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ECommerce.Domains.Services;
using ECommerce.Domains.Repositories;
using ECommerce.Domains.Models;

namespace ECommerce.Services
{
     /*
    * Class handles all the logic for controllers
    *
    * TODO: move CartItems methods to seperate CartItemsService class
    */
    public class CartService : ICartService
    {
        private readonly ICartRepository _cartRepository;
        private readonly IProductRepository _productRepository;

        public CartService(ICartRepository cartRepository, IProductRepository productRepository)
        {
            this._cartRepository = cartRepository;
            this._productRepository = productRepository;
        }

        public async Task<Cart> GetAsync(int id)
        {
            if(id==0) {
                return null;
            }
            return await _cartRepository.GetAsync(id);
        }

        public async Task<CartResponse> AddItemAsync(int id, int productId)
        {
            if(id==0 || productId ==0) 
                return new CartResponse("invalid params.");

            var existingCart = await _cartRepository.FindByIdAsync(id);

            if (existingCart == null) 
                return new CartResponse("Cart not found.");

            var existingItem = await _cartRepository.GetItemByProductIdAsync(productId);

            if(existingItem != null) {
                existingItem.Quantity++;
                _cartRepository.Update(existingItem);
                return new CartResponse(existingItem);
            }

            var prod = await _productRepository.GetAsync(productId);

            if(prod == null) 
                return new CartResponse("product with id:"+productId+" not found");

            try {
                var newItem = new CartItem() {CartId=id, ProductId=productId, Quantity=1};
                _cartRepository.AddItemAsync(newItem);

                return new CartResponse(newItem);
            }
            catch (Exception ex) {
                // Do some logging stuff
		        return new CartResponse($"An error occurred while adding to the cart: {ex.Message}");
            }
        }

        public async Task<CartResponse> RemoveItemAsync(int id, int itemId) 
        {
            if(id==0 || itemId ==0) 
                return new CartResponse("invalid params.");

            var existingCart = await _cartRepository.FindByIdAsync(id);

            if (existingCart == null) 
                return new CartResponse("Cart not found.");

            var existingItem = await _cartRepository.GetItemAsync(itemId);

            if (existingCart == null) 
                return new CartResponse("cart item not found.");

            try {
                 _cartRepository.RemoveItemAsync(existingItem);
                return new CartResponse(existingItem);
            }
            catch (Exception ex) {
                // Do some logging stuff
		        return new CartResponse($"An error occurred while removing from the cart: {ex.Message}");
            }
        }

        public async Task<IEnumerable<CartItem>> GetItemsAsync(int id) 
        {
            if(id==0)
                return null;

            return await _cartRepository.GetItemsAsync(id);
        }
    }
}