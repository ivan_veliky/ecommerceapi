using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ECommerce.Domains.Services;
using ECommerce.Domains.Repositories;
using ECommerce.Domains.Models;

namespace ECommerce.Services 
{
      /*
    * Class handles all the logic for controllers
    */
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            this._productRepository = productRepository;
        }

        public async Task<IEnumerable<Product>> GetAllAsync() 
        {
            return await _productRepository.GetAllAsync();
        }

        public async Task<Product> GetAsync(int id) 
        {
            if(id ==0 )
                return null;
            return await _productRepository.GetAsync(id);
        }

        public async Task<IEnumerable<Product>> GetByCategory(string category) 
        {
             if("".Equals(category)) {
                return null;
            }
            return await _productRepository.GetByCategory(category);
        }
    }
}