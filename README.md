# E-commerce Web Api 

Simple E-commerce api built with Asp.Net 

## Development server

clone project and run `dotnet run` from command prompt in root folder of the project

## Note 

after api server was set correctly you can proceed with ecommerce client here: https://gitlab.com/ivan_veliky/ecommerceclient

## Endpoints examples

`GET api/products/{ProductId}` - get product by (exaple id=1000) <br>
`GET api/products/byCategory?name={CategoryName}`- get product by category (exaple name=Laptops)<br>

`GET api/categories` - list all categories<br>

Note: there is only cart with id=1 should be used as no users adding supported <br>
`GET api/cart/{id}/getItems` - get items in cart(id=1) <br>
`POST api/cart/1/addItem/{ProductId}` - add item to cart<br>
`DELETE api/cart/1/deleteItem/{CartItemId}` - delete item from cart<br>

example response for `GET api/products`:<br>

>  [
    {
        "id": 1000,
        "name": "Huawei P30",
        "price": 0,
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        "pic": null,
        "category": {
            "id": 100,
            "name": "Smartphones"
        }
    },
    ...