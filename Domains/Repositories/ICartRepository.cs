using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Domains.Models;
using ECommerce.Domains.Services;

namespace ECommerce.Domains.Repositories 
{
    public interface ICartRepository
    {
        Task<Cart> GetAsync(int id);
        Task<CartItem> GetItemAsync(int id);

        Task<IEnumerable<CartItem>> GetItemsAsync(int id);
        Task<CartItem> GetItemByProductIdAsync(int prodId);
        void AddItemAsync(CartItem item);
        void Update(CartItem item);
        Task<Cart> FindByIdAsync(int id);
        void RemoveItemAsync(CartItem item);
    }
}
