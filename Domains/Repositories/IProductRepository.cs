using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Domains.Models;

namespace ECommerce.Domains.Repositories 
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllAsync();
        Task<Product> GetAsync(int id);
        Task<IEnumerable<Product>> GetByCategory(string category);
    }
}
