using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Domains.Models
{
    public class Cart { 
        public int Id { get; set; } 
        public float Total { get;  set; }
        public IList<CartItem> CartItems { get; set; } = new List<CartItem>();
    }
}
