using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Domains.Models
{
     public class Product { 
        public int Id { get; set; } 
        public string Name { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }
        public string Pic { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        //public IList<Cart> Carts { get; set; } = new List<Cart>();
        public IList<CartItem> CartItems { get; set; } = new List<CartItem>();
        
    }
}