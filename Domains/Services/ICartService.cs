using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Domains.Models;

namespace ECommerce.Domains.Services 
{
    public interface ICartService
    {
        Task<Cart> GetAsync(int userId);
        Task<IEnumerable<CartItem>> GetItemsAsync(int id);
        Task<CartResponse> AddItemAsync(int id, int productId);
        Task<CartResponse> RemoveItemAsync(int id, int productId);
    }
}
