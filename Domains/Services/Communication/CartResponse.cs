using ECommerce.Domains.Models;

namespace ECommerce.Domains.Services
{
    public class CartResponse: BaseResponse
    {
        public CartItem item { get; private set; }

        public CartResponse(bool success, string message, CartItem item): base(success, message)
        {
            this.item = item;
        }

        public CartResponse(CartItem item): this (true, string.Empty, item) 
        { }

        public CartResponse(string message): this (false, message, null) 
        { }
    }
}