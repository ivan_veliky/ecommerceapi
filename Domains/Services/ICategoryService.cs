using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Domains.Models;

namespace ECommerce.Domains.Services 
{
    public interface ICategoryService
    {
        Task<IEnumerable<Category>> GetAllAsync();
        Task<Category> GetAsync(int id);
        Task<IEnumerable<Product>> GetProducts(int catId);
    }
}
