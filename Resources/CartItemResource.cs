using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerce.Resources
{
     public class CartItemResource  { 
        public int Id { get; set; } 
        public int CartId { get; set; }
        public int Quantity { get; set; } 
        public ProductResource Product { get; set; }

    }
}