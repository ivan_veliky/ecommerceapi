namespace ECommerce.Resources 
{
    public class ProductResource 
    {
        public int Id { get; set; } 
        public string Name { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }
        public string Pic { get; set; }
        public CategoryResource Category { get; set; }
    }
}