using System.Collections.Generic;
using ECommerce.Domains.Models;

namespace ECommerce.Resources
{
    public class CategoryResource 
    {
        public int Id { get; set; }
        public string name { get; set; }
    }
}
