using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ECommerce.Domains.Models;

namespace ECommerce.Resources
{
    public class CartResource { 
        public int Id { get; set; } 
        public float Total { get; set; }
        public IList<CartItemResource> CartItems { get; set; } = new List<CartItemResource>();
    }
}
