using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

using ECommerce.Domains.Services;
using ECommerce.Domains.Models;
using ECommerce.Resources;

namespace ECommerce.Controllers
{
    [Route("/api/[controller]")]
    public class CartController : Controller
    {
        private readonly ICartService _cartService;
        private readonly IMapper _mapper;

        public CartController(ICartService cartService, IMapper mapper) 
        {
            _cartService = cartService;
            _mapper = mapper;
        }

        // GET api/cart/1
        [HttpGet("{userId}")]
        public async Task<CartResource> GetAsync(int userId) 
        {
            var cart = await _cartService.GetAsync(userId);
            var resources = _mapper.Map<Cart,CartResource>(cart);

            return resources;
        }

        // GET api/cart/1/getItems
        [HttpGet("{userId}/getItems")]
        public async Task<IEnumerable<CartItemResource>> GetItemsAsync(int userId) 
        {
            var cart = await _cartService.GetItemsAsync(userId);
            var resources = _mapper.Map<IEnumerable<CartItem>,IEnumerable<CartItemResource>>(cart);

            return resources;
        }


        // POST api/cart/1/addItem/1000
        [HttpPost("{userId}/addItem/{productId}")]
        public async Task<IActionResult> AddItemAsync(int userId, int productId) {
            var result = await _cartService.AddItemAsync(userId, productId);

            if (!result.Success) {
                return BadRequest(result.Message);
            }
            var resource = _mapper.Map<CartItem, CartItemResource>(result.item);
            return Ok(resource);
        }

        // DELETE api/cart/1/deleteItem/10
        [HttpDelete("{userId}/removeItem/{itemId}")]
        public async Task<IActionResult> RemoveItemAsync(int userId, int itemId)
        {
            var result = await _cartService.RemoveItemAsync(userId, itemId);

            if (!result.Success) {
                return BadRequest(result.Message);
            }
            var resource = _mapper.Map<CartItem, CartItemResource>(result.item);
            return Ok(resource);
        }
    }

}
