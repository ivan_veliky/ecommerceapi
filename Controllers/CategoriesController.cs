using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

using ECommerce.Domains.Services;
using ECommerce.Domains.Models;
using ECommerce.Resources;

namespace ECommerce.Controllers
{
    [Route("/api/[controller]")]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public CategoriesController(ICategoryService categoryService, IMapper mapper) 
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }

        // GET api/categories
        [HttpGet]
        public async Task<IEnumerable<CategoryResource>> GetAllAsync() 
        {
            var categories = await _categoryService.GetAllAsync();
            var resources = _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryResource>>(categories);

            return resources;
        }

        // GET api/categories/100
        [HttpGet("{id}")]
        public async Task<CategoryResource> GetAsync(int id) 
        {
            var category = await _categoryService.GetAsync(id);
            var resource = _mapper.Map<Category, CategoryResource>(category);

            return resource;
        }

        // GET api/categories/100/getProducts
        [HttpGet("{catId}/getProducts")]
        public async Task<IEnumerable<ProductResource>> GetProducts(int catId) 
        {
            var products = await _categoryService.GetProducts(catId);
            var resources = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductResource>>(products);

            return resources;
        }
    }
}
