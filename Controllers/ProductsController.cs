using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

using ECommerce.Domains.Services;
using ECommerce.Domains.Models;
using ECommerce.Resources;

namespace ECommerce.Controllers
{
    [Route("/api/[controller]")]
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductsController(IProductService productService, IMapper mapper) 
        {
            _productService = productService;
            _mapper = mapper;
        }

        // GET api/products
        [HttpGet]
        public async Task<IEnumerable<ProductResource>> GetAllAsync() 
        {
            var products = await _productService.GetAllAsync();
            var resources = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductResource>>(products);

            return resources;
        }

        // GET api/products/1000
        [HttpGet("{id}")]
        public async Task<ProductResource> GetAsync(int id)
        {
            var product = await _productService.GetAsync(id);
            var resource = _mapper.Map<Product, ProductResource>(product);

            return resource;
        }

        // GET api/products/byCategory?name=Laptops
        [HttpGet("byCategory")]
        public async Task<IEnumerable<ProductResource>> GetByCategory(string name)
        {   
            var products = await _productService.GetByCategory(name);
            var resources = _mapper.Map<IEnumerable<Product>, IEnumerable<ProductResource>>(products);

            return resources;
        }
    }
}
